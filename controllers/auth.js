const { response, json } = require("express");
const {Usuario} = require('../models');
const bcrypt = require("bcryptjs");
const { generarJWT } = require("../helpers/generarjwt");
const { googleVerify } = require('../helpers/google-verify');

const loginController = async (req, res = response) => {

    const { correo, password } = req.body;

    try {
        //Verificar si existe email
        const usuario = await Usuario.findOne({ correo });
        if (!usuario) {
            return res.status(400).json({
                msg: 'Usuario - Pass no son correctos --correo'
            })
        }

        //verificar si estado es true
        if (!usuario.estado) {
            return res.status(400).json({
                msg: 'El usuario no se encuentra activo en Base de datos'
            })
        }
        const validPassword = bcrypt.compareSync(password, usuario.password);
        //verificar la contraseña
        if (!validPassword) {
            return res.status(400).json({
                msg: 'Usuario - Pass no son correctos -- pass'
            });
        }

        //generar JWT
        const token = await generarJWT(usuario.id);

        res.json({
            usuario,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Hable con el admin',
        })
    }


}
const googleSingIn = async (req, res = response) => {

    const { id_token } = req.body;

    try {
        const { nombre, img, correo, rol} = await googleVerify(id_token);

        let usuario = await Usuario.findOne({ correo })
        if (!usuario) {
            const data = {
                nombre,
                correo,
                password: ':p',
                img,
                rol: 'USER_ROLE',
                google: true
            }
            usuario = new Usuario(data);
            await usuario.save();
        }

        //SI el usaurio en DB
        if (!usuario.estado) {
            return res.status(401).json({
                msg: 'Hable con el administrador, usuario bloqueado'
            });
        }
        const token = await generarJWT(usuario.id);

       
        res.json({
            usuario, token
        });


    } catch (error) {
        console.log(error);
        res.status(400).json({
            msg: 'El token no se pudo verificar'
        })
    }
}


module.exports = {
    googleSingIn,
    loginController, 
}