const { response } = require("express");
const { Usuario, Categoria, Producto } = require("../models");
const { ObjectId } = require('mongoose').Types;

const categoriasPermitidas = [
    'usuarios',
    'categorias',
    'productos',
    'roles'
];

const buscarUsuarios = async (termino = '', res = response) => {
    const esMongoId = ObjectId.isValid(termino);
    if (esMongoId) {
        const usuario = await Usuario.findById(termino);
        return res.json({
            results: (usuario) ? [usuario] : []
        });
    };

    const regex = new RegExp(termino, 'i');

    const usuario = await Usuario.find({
        $or: [{ nombre: regex }, { correo: regex }],
        $and: [{ estado: true }]
    });

    res.json({
        results: usuario
    })
};

const buscarCategorias = async (nombre = '', res = response) => {
    const esMongoId = ObjectId.isValid(nombre);
    if (esMongoId) {
        const categoria = await Categoria.find(nombre)
        .populate('usuario', 'nombre');
        return res.json({
            results: (categoria) ? [categoria] : []
        });
    };

    const regexCat = new RegExp(nombre, 'i');

    const categorias = await Categoria.find({ nombre: regexCat, estado: true })
    .populate('usuario', 'nombre');

    res.json({
        results: categorias
    })


};
const buscarProductos = async (nombre = '', res = response) => {
    const esMongoId = ObjectId.isValid(nombre);
    if (esMongoId) {
        const producto = await Producto.find(nombre)
        .populate('categoria', 'nombre')
        .populate('usuario', 'nombre');
        return res.json({
            results: (producto) ? [producto] : []
        });
    };

    const regexPro = new RegExp(nombre, 'i');

    const productos = await Producto.find({ nombre: regexPro, estado: true })
    .populate('categoria', 'nombre');

    res.json({
        results: productos
    })


};
const busqueda = (req, res = response) => {

    const { categoria, producto } = req.params;

    if (!categoriasPermitidas.includes(categoria)) {
        return res.status(400).json({
            msg: `las categorias permitidas son: ${categoriasPermitidas}`
        });
    };

    switch (categoria) {
        case 'usuarios':
            buscarUsuarios(producto, res);
            break;
        case 'categorias':
            buscarCategorias(producto, res);
            break;
        case 'productos':
            buscarProductos(producto, res);
            break;

        default:
            res.status(500).json({
                msg: 'Se me olvido hacer esta busqueda'
            });
    };
}

module.exports = {
    busqueda
}