const { request, response } = require("express");
const { Categoria } = require("../models");

//actualizar
const actualizarCategoria = async (req = request, res = response) => {
   
    try {
        const { id } = req.params;
        const { estado, usuario, ...data } = req.body;
        data.nombre = data.nombre.toUpperCase();
        data.usuario = req.usuario._id;

        const categoria = await Categoria.findByIdAndUpdate(id, data, { new: true });

        res.status(201).json({
            categoria
        })
    } catch (err) {
        console.log(err);
        throw new Error(`El ID: ${id} no existe en DB`);
    };
};

//estado: false
const borrarCategoria = async (req = request, res = response) => {
    try {
        const { id } = req.params;
        const categoria = await Categoria.findByIdAndUpdate(id, { estado: false }, { new: true });

        res.status(200).json({
            categoria
        })
    } catch (err) {
        console.log(err);
        throw new Error(`El ID: ${id} no existe en DB`);
    }

}

//crear categoria
const createCategoria = async (req = request, res = response) => {
    try {

        const nombre = req.body.nombre.toUpperCase();
        const categoriaDB = await Categoria.findOne({ nombre });

        if (categoriaDB) {
            return res.status(400).json({
                msg: `La categoria ${nombre} ya existe en BD`
            })
        };

        //Generar la data a guardar
        const data = {
            nombre,
            usuario: req.usuario._id,
        }

        const categoria = new Categoria(data);

        //Guardar en BD
        await categoria.save()

        res.status(201).json(categoria);
    } catch (error) {
        console.log(error);
        throw new Error(`El ID: ${Categoria.id} no existe en DB`)
    }
}
//populate {}
const obtenerCategoria = async (req = request, res = response) => {
    try {
        const { id } = req.params;
        const categoria = await Categoria.findById(id).populate('usuario', 'nombre');

        res.status(200).json({
            categoria
        })
    } catch (error) {
        console.log(err);
        throw new Error(`El ID: ${id} no existe en DB`)
    }
}
//paginado - total -populate
const obtenerCategorias = async (req = request, res = response) => {
    try {
        const { desde = 0, limite = 10 } = req.query;
        const categoriaTrue = { estado: true };

        const [total, categorias] = await Promise.all([
            Categoria
                .count(categoriaTrue),
            Categoria
                .find(categoriaTrue)
                .populate('usuario', 'nombre')
                .skip(Number(desde))
                .limit(Number(limite))
        ]);

        res.status(200).json({
            total, categorias
        });

    } catch (error) {
        console.log(error);
        throw new Error('No se encontraron Categorias');
    }

}





module.exports = {
    actualizarCategoria,
    borrarCategoria,
    createCategoria,
    obtenerCategoria,
    obtenerCategorias,
}