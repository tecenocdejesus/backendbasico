const Auth = require('./auth');
const Buscar = require('./buscar');
const Categoria = require('./categorias');
const Producto = require('./productos');
const Usuario = require('./usuarios');
const Upload = require('./uploads');


module.exports= {
    Auth,
    Buscar,
    Categoria,
    Producto,
    Usuario,   
    ...Upload
}