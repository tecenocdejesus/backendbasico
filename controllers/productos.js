const { response } = require("express");
const { Producto } = require("../models");

const actualizarProducto = async (req, res = response) => {
    try {
        const { id } = req.params;
        const { estado, usuario, ...data } = req.body;
        if (data.nombre) {
            data.nombre = data.nombre.toUpperCase();
            
        }
        data.usuario = req.usuario._id;
    
        const producto = await Producto.findByIdAndUpdate(id, data, {new: true});
        
        res.status(201).json({
            producto
        })
    } catch (err) {
        throw new Error('No se actualizo el producto')
    }
}

const borrarProducto = async (req, res = response) => {
   try {
    const {id} = req.params;
    const productoBorrado = await Producto.findByIdAndUpdate(id, {estado: false}, {new: true})

    res.json(productoBorrado)
   } catch (err) {
    console.log(err);
    throw new Error ('No se elimino el Producto')
   }
}

const crearProducto = async (req, res = response) => {
    try {
        const {estado, usuario, ...body } = req.body;
        const nombre = req.body.nombre.toUpperCase();
        const productoDB = await Producto.findOne({ nombre });
        if (productoDB) {
            return res.status(400).json({ 
                msg: `El producto con nombre ${productoDB.nombre} ya existe en la base de datos` })
        }
        //Generar la data a guardar
        const data = {
            ...body,
            nombre: body.nombre.toUpperCase(),
            usuario: req.usuario._id
        }
        const producto = new Producto(data);
        //guardar en db
        await producto.save();
        res.status(201).json({ producto });
    } catch (error) {
        console.log(error);
        throw new Error('No se creo el producto');
    }

}
const obtenerProductoPorId = async (req, res = response) => {
    try {
        const { id } = req.params;
        const producto = await Producto.findById(id)
        .populate('usuario', 'nombre')
        .populate('categoria', 'nombre');

        res.status(200).json(producto);
    } catch (error) {
        console.log(error);
        throw new Error('No se encontro Producto por ID')
    }


}
const obtenerProductos = async (req, res = response) => {

    try {
        const { limite = 5, desde = 0 } = req.query;
        const productosActivos = { estado: true };

        const [total, productos] = await Promise.all([
            Producto.countDocuments(productosActivos),
            Producto.find(productosActivos)
                .populate('usuario', 'nombre')
                .populate('categoria', 'nombre')
                .skip(Number(desde))
                .limit(Number(limite))
        ]);
        res.status(200).json({ total, productos })
    } catch (error) {
        console.log(error);
        throw new Error('No existe producto')
    }
}



module.exports = {
    actualizarProducto,
    borrarProducto,
    crearProducto,
    obtenerProductoPorId,
    obtenerProductos,
}