const { Usuario } = require('../models');
const { response, request } = require("express");
const bcrypt = require('bcryptjs');

const usuariosDelete = async (req, res = response) => {

    const { id } = req.params;

    //borrado fisicamente
    //const usuario = await Usuario.findByIdAndDelete(id);
    //Con este si se hace se actualiza solo el estado 
    const usuario = await Usuario.findByIdAndUpdate(id, { estado: false })
    res.json(usuario);
};

const usuarioGet = async (req, res = response) => {
    const { id } = req.params;
    const usuario = await Usuario.findOne({ id });

    res.json({
        usuario
    });
};

const usuariosGet = async (req = request, res = response) => {
    const { desde = 1, limite = 10 } = req.query;
    const query = { estado: true };

    const [total, usuarios] = await Promise.all([
        Usuario
            .count(query),
        Usuario
            .find(query)
            .limit(Number(limite))
            .skip(Number(desde))
    ]);

    res.json({
        total, usuarios
    });
};

const usuariosPost = async (req, res = response) => {

    const { nombre, correo, password, rol } = req.body;
    const usuario = new Usuario({ nombre, correo, password, rol });

    //encriptar o hashear la contra
    const hash = bcrypt.genSaltSync(8);
    usuario.password = bcrypt.hashSync(password, hash);

    //guardar en db
    await usuario.save();
    res.json({
        usuario
    });
};

const usuariosPut = async (req = request, res = response) => {
    const { id } = req.params;
    const { _id, password, google, ...resto } = req.body;

    //encriptar o hashear la contra
    const hash = bcrypt.genSaltSync(8);
    resto.password = bcrypt.hashSync(password, hash);

    const usuario = await Usuario.findByIdAndUpdate(id, resto);
    res.json({
        msg: 'put API',
        usuario
    });
};

module.exports = {
    usuariosDelete,
    usuarioGet,
    usuariosGet,
    usuariosPost,
    usuariosPut,
}