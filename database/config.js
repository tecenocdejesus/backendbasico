const mongoose = require('mongoose');

const dbConnection = async () => {

    try {
        const DB_URI = process.env.DB_URI;
        await mongoose.connect(DB_URI);

        console.log('Base de datos Online');
    } catch (error) {
        throw new Error('Error al conectar con la BD');
    }
}

module.exports = {
    dbConnection
}