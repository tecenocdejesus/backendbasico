const Role = require("../models/role");
const { Usuario, Categoria, Producto } = require("../models");

//verificar que el correo existe
const emailExiste = async (correo = '') => {
    const existeEmail = await Usuario.findOne({ correo });
    if (existeEmail) {
        throw new Error(`El correo ${correo} ya esta registrado`);
    }
};

const esRoleValido = async (rol = '') => {
    const existeRole = await Role.findOne({ rol });
    if (!existeRole) {
        throw new Error(`El rol ${rol} no esta registrado en BD`);
    }
}

const existeCategoriaPorID = async (id) => {
    const existeCat = await Categoria.findById(id);
    if (!existeCat) {
        throw new Error(`El id ${id} no existe en BD`);
    }
}

//verificar que el ususario existe
const existeUsuarioPorID = async (id = '') => {
    const existeUsuario = await Usuario.findById(id);
    if (!existeUsuario) {
        throw new Error(`El id ${id} no existe`);
    }
}

const existeProductoPorId = async (id) => {
    const productoExistente = await Producto.findById(id);
    if (!productoExistente) {
        throw new Error(`El id ${id} no existe en BD`);
    }
}

const coleccionesPermitidas = (coleccion = '', colecciones = []) => {

    const incluida = colecciones.includes(coleccion);
    if (!incluida) {
        throw new Error(`la Coleccion ${coleccion}, no es permitida, ${colecciones}`)
    }
    return true;
}

module.exports = {
    emailExiste,
    esRoleValido,
    existeCategoriaPorID,
    existeUsuarioPorID,
    existeProductoPorId,
    coleccionesPermitidas
}