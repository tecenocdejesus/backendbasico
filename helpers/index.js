const DBValidator = require('./db-validators');
const GoogleVerify = require('./google-verify');
const GenerarJWT = require('./generarjwt');
const subirArchivo = require('./subir-archivo');

module.exports= {
    DBValidator,
    GoogleVerify,
    GenerarJWT,
    ...subirArchivo
}