const esAdminRole = require("./validar-roles");
const validarCampos = require("./validar-campos");
const validarJWT = require("./validar-jwt");
const validarArchivoSubido = require('./validarArchivoSubir')

module.exports={
    ...esAdminRole,
    ...validarCampos,
    ...validarJWT,
    ...validarArchivoSubido
}
