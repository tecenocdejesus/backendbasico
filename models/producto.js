const { Schema, model } = require('mongoose');

const ProductoSchema = Schema({

    categoria: {
        type: Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true
    },

    descripcion: { type: String },

    disponible: { type: Boolean, default: true },
    
    img: {type: String},

    estado: {
        type: Boolean,
        default: true,
        required: true
    },

    nombre: {
        type: String,
        required: [true, 'El nombre es Obligatorio'],
        unique: true
    },

    precio: {
        type: Number,
        default: 0
    },

    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },

});

ProductoSchema.methods.toJSON = function () {
    const { __v, estado, _id, ...data } = this.toObject();
    data.uid = _id;
    return data;
}

module.exports = model('Producto', ProductoSchema);

