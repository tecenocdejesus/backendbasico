const { Router } = require('express');
const { check } = require('express-validator');
const {Auth} = require('../controllers')
const { validarCampos } = require('../middlewares/');


const router = Router();

router.post('/google',[
    check('id_token', 'El ID_token Es requerido').not().isEmpty(),
    validarCampos
], Auth.googleSingIn);

router.post('/login',[
    check('password', 'La contraseña es obligatoria').not().isEmpty(),
    check('correo', 'El correo es obligatorio').isEmail(),
    validarCampos
], Auth.loginController);

router.post('/register');




module.exports= router;