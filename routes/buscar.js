const { Router } = require("express");
const { Buscar } = require("../controllers");

const router = Router()

router.get('/:categoria/:producto', Buscar.busqueda)

module.exports= router;