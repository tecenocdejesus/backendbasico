const { Router } = require('express');
const { check } = require('express-validator');
const { Categoria } = require('../controllers');
const { validarJWT, validarCampos, esAdminRole } = require('../middlewares');
const { DBValidator } = require('../helpers');

const router = Router();

//eliminar categoria solo si es admin
router.delete('/:id', [
    validarJWT,
    esAdminRole,
    check('id', 'No es un Id de Mongo Valido').isMongoId(),
    check('id').custom(DBValidator.existeCategoriaPorID),
    validarCampos
], Categoria.borrarCategoria);

//obtener todas las categorias
router.get('/', validarJWT, Categoria.obtenerCategorias);

//obtener una categoria por id
router.get('/:id', [
    check('id', 'No e un Id de Mongo Valido').isMongoId(),
    check('id').custom(DBValidator.existeCategoriaPorID),
    validarCampos
], Categoria.obtenerCategoria);

//crear una nueva categoria privado con un rol y token valido
router.post('/', [validarJWT,
    check('nombre', 'El nombre es Obligatorio'),
    validarCampos
], Categoria.createCategoria);

//actualizar una categoria por id
router.put('/:id', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('id').custom(DBValidator.existeCategoriaPorID),
    validarCampos
], Categoria.actualizarCategoria);

module.exports = router;