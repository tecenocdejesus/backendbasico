const { check } = require('express-validator');
const { Router } = require('express');
const { Producto, cargarArchivo } = require('../controllers');
const { validarCampos, validarJWT, esAdminRole, validarArchivoSubido } = require('../middlewares');
const {DBValidator} = require('../helpers');

const router = Router();

router.delete('/:id',[
    validarJWT,
    esAdminRole,
    check('id', 'No es un ID de Mongo Valido').isMongoId(),
    check('id').custom(DBValidator.existeProductoPorId),
    validarCampos
], Producto.borrarProducto);

router.get('/', validarJWT, Producto.obtenerProductos);
//Obtener Producto por ID --Publico

router.get('/:id', [
    check('id','No es un id de Mongo Valido').isMongoId(),
    check('id').custom(DBValidator.existeProductoPorId), 
    validarCampos
], Producto.obtenerProductoPorId);

//Crear Producto - Privado - Cualquier Persona con token valido
router.post('/',[
    validarJWT, 
    check('nombre', 'El nombre es obligatorio').notEmpty(),
    check('categoria', 'No es un ID de mongo valido').isMongoId(),
    check('categoria').custom(DBValidator.existeCategoriaPorID),
    validarArchivoSubido,
    cargarArchivo,
    validarCampos
], Producto.crearProducto);
//actualizar estado producto, jwt, categoria
router.put('/:id', [
    validarJWT, 
   // check('categoria', 'No es un ID de mongo valido').isMongoId(),
    check('id').custom(DBValidator.existeProductoPorId),
    validarCampos
], Producto.actualizarProducto);

module.exports = router;