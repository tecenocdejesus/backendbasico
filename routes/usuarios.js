const { Router } = require("express");
const {
    Usuario
} = require('../controllers');
const { check } = require('express-validator');
const { 
    emailExiste,
    esRoleValido,
    existeUsuarioPorID,
} = require("../helpers/db-validators");

const {
    esAdminRole,
    tieneRole,
    validarCampos,
    validarJWT,
} = require('../middlewares');

const router = Router();

router.delete('/:id',[
    validarJWT,
    esAdminRole,
    tieneRole('ADMIN_ROLE', 'VENTAS_ROLE'),
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existeUsuarioPorID),
    validarCampos
],  Usuario.usuariosDelete);

router.get('/', Usuario.usuariosGet);

router.get('/:id', Usuario.usuarioGet);

router.post('/', [
    check('nombre', 'el nombre es requerido').not().isEmpty(),
    check('password', 'la contraseña es requerida minimo 6 caracteres').isLength({ min: 6 }),
    check('correo', 'El correo no es valido').isEmail(),
    check('correo').custom(emailExiste),
    check('rol').custom( esRoleValido),
    validarCampos
], Usuario.usuariosPost);

router.put('/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existeUsuarioPorID),
    check('rol').custom( esRoleValido),
    validarCampos
], Usuario.usuariosPut);

module.exports = router;